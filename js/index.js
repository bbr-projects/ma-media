(function () {
  "use strict";
  var menuId;
  function init () {
    menuId = document.getElementById("menu");
    document.addEventListener("scroll",scrollMenu,false);
  }
  function scrollMenu () {
    if (document.documentElement.scrollTop > 100) {
      menuId.classList.add("scroll");
      // console.log('scroll');
    }
    else {
      menuId.classList.remove("scroll");
      // console.log('no-scroll');
    }
  }
  document.addEventListener("DOMContentLoaded",init,false);
})();





$num = $('.my-card').length;
$even = $num / 2;
$odd = ($num + 1) / 2;

if ($num % 2 == 0) {
  $('.my-card:nth-child(' + $even + ')').addClass('active');
  $('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
  $('.my-card:nth-child(' + $even + ')').next().addClass('next');
} else {
  $('.my-card:nth-child(' + $odd + ')').addClass('active');
  $('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
  $('.my-card:nth-child(' + $odd + ')').next().addClass('next');
}

// $('.left').click(function(){
//   $('.card-carousel').stop(false, true).animate({left: '+=' + $slide});
// });

$('.my-card').click(function() {
  $slide = $('.active').width();
  // console.log($('.active').position().left);

  if ($(this).hasClass('next')) {
    $('.card-carousel').stop(false, true).animate({left: '-=' + $slide});

  } else if ($(this).hasClass('prev')) {
    $('.card-carousel').stop(false, true).animate({left: '+=' + $slide});
  }






  $(this).removeClass('prev next');
  $(this).siblings().removeClass('prev active next');

  $(this).addClass('active');
  $(this).prev().addClass('prev');
  $(this).next().addClass('next');
});


// $('.left').click(function() { // left
//   $('.active').prev().trigger('click');
// });


$('.left').click(function(){
  $('.active').prev().trigger('click');
});

$('.right').click(function(){
  $('.active').next().trigger('click');
})


// Keyboard nav
$('html body').keydown(function(e) {
  if (e.keyCode == 37) { // left
    $('.active').prev().trigger('click');
  }
  else if (e.keyCode == 39) { // right
    $('.active').next().trigger('click');
  }
});
